--------------
2.1 Egenmakt
--------------

**Piratpartiet värnar varje individs möjlighet att ha makt och inflytande över sin egen livssituation**, sina arbetsuppgifter, sin närmiljö, sin lokalpolitik et cetera. Vi kallar detta för egenmakt. Varje individ ska ha rätten att själv fatta beslut om sitt liv, utan att dessa beslut behöver vara korrekta enligt någon annan. Vi tror att varje individ själv är bäst lämpad att bedöma konsekvenserna av sina val. 

För att individer ska ha en faktisk möjlighet att styra över sina liv behöver de också få uppmuntran och stöd i att lära sig de kunskaper och de erfarenheter som krävs för att vara - i bred mening - självförsörjande eller självtillräckliga. Individer ska få det stöd som krävs för att de sedan på egen hand kan överkomma problem i sina liv. Människor har av naturen **agens**, vad vi politiskt behöver göra är att släppa fram den. Så får vi ett starkare, friare samhälle.

Inte bara individer kan utöva egenmakt, utan även gemenskaper och samhällen av egenmäktiga individer. När individer som själva är **autonoma** och självtillräckliga kan samarbeta i gemenskaper har de större möjlighet att ta kontroll över sina villkor, utöva makt och uppnå sina gemensamma mål. Dessa gemenskaper är vad vi kallar **svärmar**. De kan vara allt från ett fackförbund, en bostadsrättsförening, en fotbollsklubb, till gruppen som utvecklar ett open source-projekt.

**Den som förlorar sin egenmakt till en avlägsen, centraliserad, tvingande institution tar skada av detta**. Det inducerar passivitet, leda, nedsatt hälsa, depression och så vidare. Den som upplever att den inte har möjlighet att påverka sin situation lär sig att vara hjälplös, istället för en aktivt deltagande samhällsmedborgare.

Piratpartiet motarbetar därför alla tendenser till att medborgaren ska tvingas luta sig tillbaka som en passiv, desillusionerad person. Vi värderar istället högt att individer ska kunna utöva sin egenmakt som aktiva samhällsmedborgare. Vår politiska uppgift är att skapa förutsättningar för egenmakt genom att förstärka individens **frihet** och **värdighet**, och genom att förbättra möjligheterna till **deltagande**. Då har ansvarstagande och aktiva individer möjligheter att skapa egenmakt för sig och sina svärmar.
