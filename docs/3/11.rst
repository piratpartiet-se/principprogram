3.11 Patent och andra idémonopol 
-----------------------------------

Starka ekonomiska aktörer lägger idag hinder för en fri användning av kunskap, idéer och information. Patent, växtsortsskydd, designskydd, varumärkesskydd och annan immaterialrätt sägs ha skapats för att stimulera och underlätta innovationer och utveckling. I själva verket är de ofta reliker från en tid med en starkt reglerad och styrd ekonomi, som försvårar innovation och utveckling och skapar onödiga globala orättvisor.

När många företag idag använder patent på närmast självklara saker för att skrämma bort konkurrenter blir det riskabelt för entreprenörer och företag att utveckla teknik och de tvingas ägna lika stora resurser åt immaterialrättslig juridik som åt forskning och utveckling. Det är slöseri med resurser. Det hämmar innovationer. Det gynnar stora etablerade aktörer, och skapar en besvärlig situation för de små och medelstora företag, där många verkligt intressanta innovationer sker.

Idémonopol är också problematiska ur ett rent humanitärt perspektiv. När idémonopolister tar ut höga priser för utveckling av exempelvis nya läkemedel eller grödor är priset ofta satt efter betalningförmågan i de rika länderna. Detta innebär att människor i fattigare delar av världen utestängs från många av utvecklingens frukter: De bästa medicinerna och härdigaste eller nyttigaste grödorna blir ett privilegium för människorna i den rika delen av världen. Friare kunskap och utnyttjande av innovationer bidrar därför till att minska ekonomiska klyftor både inom och mellan länder.

Patent skaffas idag inte i syfte att skydda uppfinningar, utan för att utvinna monopolräntor. Särskilt tydligt är det inom den medicinska branschen, där det allmänna står för en stor del av finansieringen av ny medicin, medan det privatas budget för utveckling är betydligt mindre än den för marknadsföring. Det mesta av den privata utvecklingen sker på så kallade “me too”-patent på mediciner som är i praktiken identiska med andra existerande mediciner. Annars forskar man på små variationer av den medicin man redan säljer, i syfte att artificiellt förlänga patentet. 

Piratpartiet anser att immaterialrätt och andra inskränkningar i principen om fritt utnyttjande av information bara ska göras om det finns väldokumenterade positiva effekter som motiverar sådana inskränkningar.
