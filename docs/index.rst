"""""""""""""""""
Piratpartiets principprogram 5.0
"""""""""""""""""

.. toctree::
   :caption: 0. Piratpartiet i korthet
   :hidden:

   0

:doc:`0`
    Piratpartiet i korthet

.. toctree::
   :caption: 1. Digitaliseringen förändrar allt
   :hidden:

   1/0
   1/1
   1/2
   1/2/1
   1/2/2
   1/2/3
   1/2/4
   1/3

.. toctree::
   :caption: 2. Piratpartiets ideologi
   :hidden:

   2/1
   
.. toctree::
   :caption: 2.2 Våra värderingar
   :hidden:

   2/2/1
   2/2/2
   2/2/3

.. toctree::
   :caption: 2.3 Våra principer
   :hidden:

   2/3/1
   2/3/2
   2/3/3
   2/3/4
   2/3/5
   2/3/6

.. toctree::
   :caption: 3. Politik för kunskapssamhället
   :hidden:

   3/1
   3/2
   3/3
   3/4
   3/5
   3/6
   3/7
   3/8
   3/9
   3/10
   3/11
